Testing USB Type C (power port) connected to PC
===============================================

* [USB C Female to USB Male Adapter ](https://www.amazon.com/dp/B083XXLW77) + usb type c to type C cable
OR
* [USB C to USB Male cable](https://www.amazon.com/dp/B01CZVEUIE/)


![Type C Board](img/usb_typec_board.jpg?raw=true "Board Type C")
![Type C PC](img/usb_typec_pc.jpg?raw=true "PC Type A")

Test Procedure
--------------

Connect board power supply port as described above to PC.

On Linux PC for example, the board appears as a "BEAGLEBONE" drive.

![Screenshot](img/usb_typec_pc_view.png?raw=true "Screenshot")
