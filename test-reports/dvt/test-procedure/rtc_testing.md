RTC Testing
===========

WARNING: Makesure that the battery is mounted on board prior to this testing.
I have used [CR1220](https://www.amazon.com/gp/product/B0797NRXZY/) battery in
my testing

in each of these instructions:
* /dev/rtc points to /dev/rtc0
* /dev/rtc0 -> onboard BQ battery backed RTC (Does not loose time across poweroff)
* /dev/rtc1 -> AM62 RTC (looses time when system is powered off)

Display onboard RTC
===================

```
$ sudo hwclock --verbose --show -f /dev/rtc0
hwclock from util-linux 2.36.1
System Time: 1665556049.094290
Using the rtc interface to the clock.
Last drift adjustment done at 1665555801 seconds after 1969
Last calibration done at 1665555801 seconds after 1969
Hardware clock is on UTC time
Assuming hardware clock is kept in UTC time.
Waiting for clock tick...
ioctl(4, RTC_UIE_ON, 0): Invalid argument
Waiting in loop for time from /dev/rtc0 to change
...got clock tick
Time read from Hardware Clock: 2022/10/12 06:27:30
Hw clock time : 2022/10/12 06:27:30 = 1665556050 seconds since 1969
Time since last adjustment is 249 seconds
Calculated Hardware Clock drift is 0.000000 seconds
2022-10-12 06:27:29.008229+00:00
```

Display on SoC RTC
==================

```
$ sudo hwclock --verbose --show -f /dev/rtc1
hwclock from util-linux 2.36.1
System Time: 1665556066.184848
Using the rtc interface to the clock.
Last drift adjustment done at 1665555801 seconds after 1969
Last calibration done at 1665555801 seconds after 1969
Hardware clock is on UTC time
Assuming hardware clock is kept in UTC time.
Waiting for clock tick...
...got clock tick
Time read from Hardware Clock: 1970/01/01 00:44:49
Hw clock time : 1970/01/01 00:44:49 = 2689 seconds since 1969
Time since last adjustment is -1665553112 seconds
Calculated Hardware Clock drift is 0.000000 seconds
1970-01-01 00:44:48.492336+00:00
```

Set time on onboard RTC
=======================

```
debian@BeagleBone:~$ sudo hwclock --verbose -w -f /dev/rtc0
hwclock from util-linux 2.36.1
System Time: 1665556128.512718
Using the rtc interface to the clock.
Last drift adjustment done at 1665556125 seconds after 1969
Last calibration done at 1665556125 seconds after 1969
Hardware clock is on UTC time
Assuming hardware clock is kept in UTC time.
RTC type: 'bq32k'
Using delay: 0.000000 seconds
missed it - 1665556128.514482 is too far past 1665556128.000000 (0.514482 > 0.001000)
1665556129.000000 is close enough to 1665556129.000000 (0.000000 < 0.002000)
Set RTC to 1665556129 (1665556128 + 1; refsystime = 1665556128.000000)
Setting Hardware Clock to 06:28:49 = 1665556129 seconds since 1969
ioctl(RTC_SET_TIME) was successful.
Not adjusting drift factor because the --update-drift option was not used.
New /etc/adjtime data:
0.000000 1665556128 0.000000
1665556128
UTC
```

Set time on SoC RTC
=======================

```
debian@BeagleBone:~$ sudo hwclock --verbose -w -f /dev/rtc1
hwclock from util-linux 2.36.1
System Time: 1665556125.096508
Using the rtc interface to the clock.
Last drift adjustment done at 1665555801 seconds after 1969
Last calibration done at 1665555801 seconds after 1969
Hardware clock is on UTC time
Assuming hardware clock is kept in UTC time.
RTC type: 'rtc-ti-k3'
Using delay: 0.000000 seconds
missed it - 1665556125.098251 is too far past 1665556125.000000 (0.098251 > 0.001000)
1665556126.000000 is close enough to 1665556126.000000 (0.000000 < 0.002000)
Set RTC to 1665556126 (1665556125 + 1; refsystime = 1665556125.000000)
Setting Hardware Clock to 06:28:46 = 1665556126 seconds since 1969
ioctl(RTC_SET_TIME) was successful.
Not adjusting drift factor because the --update-drift option was not used.
New /etc/adjtime data:
0.000000 1665556125 0.000000
1665556125
UTC
```


Test time change
================

```
$ sudo hwclock --verbose -w -f /dev/rtc0;sleep 10;date;sudo hwclock --verbose --show -f /dev/rtc0
System Time: 1665556269.204641
Using the rtc interface to the clock.
Last drift adjustment done at 1665556128 seconds after 1969
Last calibration done at 1665556128 seconds after 1969
Hardware clock is on UTC time
Assuming hardware clock is kept in UTC time.
RTC type: 'bq32k'
Using delay: 0.000000 seconds
missed it - 1665556269.206367 is too far past 1665556269.000000 (0.206367 > 0.001000)
1665556270.000000 is close enough to 1665556270.000000 (0.000000 < 0.002000)
Set RTC to 1665556270 (1665556269 + 1; refsystime = 1665556269.000000)
Setting Hardware Clock to 06:31:10 = 1665556270 seconds since 1969
ioctl(RTC_SET_TIME) was successful.
Not adjusting drift factor because the --update-drift option was not used.
New /etc/adjtime data:
0.000000 1665556269 0.000000
1665556269
UTC
Wed Oct 12 06:31:20 UTC 2022
hwclock from util-linux 2.36.1
System Time: 1665556280.081153
Using the rtc interface to the clock.
Last drift adjustment done at 1665556269 seconds after 1969
Last calibration done at 1665556269 seconds after 1969
Hardware clock is on UTC time
Assuming hardware clock is kept in UTC time.
Waiting for clock tick...
ioctl(4, RTC_UIE_ON, 0): Invalid argument
Waiting in loop for time from /dev/rtc0 to change
...got clock tick
Time read from Hardware Clock: 2022/10/12 06:31:21
Hw clock time : 2022/10/12 06:31:21 = 1665556281 seconds since 1969
Time since last adjustment is 12 seconds
Calculated Hardware Clock drift is 0.000000 seconds
2022-10-12 06:31:20.077303+00:00

```


Testing with Linux kernel selftest tools
========================================

```
cd linux/tools/testing/selftests/rtc
make -j6 CROSS_COMPILE=aarch64-none-linux-gnu- ARCH=arm64'
```

this generates two binaries:
* rtctest
* setdate

NOTE: current drivers/rtc/rtc-bq32k.c driver does'nt yet support IRQs, so alarm tests will fail. but /dev/rtc1 does support alarms



```
debian@BeagleBone:~$ sudo ./rtctest /dev/rtc1
TAP version 13
1..7
# Starting 7 tests from 2 test cases.
#  RUN           rtc.date_read ...
# rtctest.c:47:date_read:Current RTC date/time is 12/10/2022 06:35:26.
#            OK  rtc.date_read
ok 1 rtc.date_read
#  RUN           rtc.uie_read ...
#            OK  rtc.uie_read
ok 2 rtc.uie_read
#  RUN           rtc.uie_select ...
#            OK  rtc.uie_select
ok 3 rtc.uie_select
#  RUN           rtc.alarm_alm_set ...
# rtctest.c:136:alarm_alm_set:Alarm time now set to 06:35:35.
# rtctest.c:156:alarm_alm_set:data: 1a0
#            OK  rtc.alarm_alm_set
ok 4 rtc.alarm_alm_set
#  RUN           rtc.alarm_wkalm_set ...
# rtctest.c:192:alarm_wkalm_set:Alarm time now set to 12/10/2022 06:35:38.
#            OK  rtc.alarm_wkalm_set
ok 5 rtc.alarm_wkalm_set
#  RUN           rtc.alarm_alm_set_minute ...
# rtctest.c:238:alarm_alm_set_minute:Alarm time now set to 06:36:00.
# rtctest.c:258:alarm_alm_set_minute:data: 1a0
#            OK  rtc.alarm_alm_set_minute
ok 6 rtc.alarm_alm_set_minute
#  RUN           rtc.alarm_wkalm_set_minute ...
# rtctest.c:294:alarm_wkalm_set_minute:Alarm time now set to 12/10/2022 06:37:00.
#            OK  rtc.alarm_wkalm_set_minute
ok 7 rtc.alarm_wkalm_set_minute
# PASSED: 7 / 7 tests passed.
# Totals: pass:7 fail:0 xfail:0 xpass:0 skip:0 error:0
```

```
debian@BeagleBone:~$ sudo ./rtctest /dev/rtc0
TAP version 13
1..7
# Starting 7 tests from 2 test cases.
#  RUN           rtc.date_read ...
# rtctest.c:47:date_read:Current RTC date/time is 12/10/2022 06:37:59.
#            OK  rtc.date_read
ok 1 rtc.date_read
#  RUN           rtc.uie_read ...
# rtctest.c:60:uie_read:skip update IRQs not supported.
#            OK  rtc.uie_read
ok 2 rtc.uie_read
#  RUN           rtc.uie_select ...
# rtctest.c:85:uie_select:skip update IRQs not supported.
#            OK  rtc.uie_select
ok 3 rtc.uie_select
#  RUN           rtc.alarm_alm_set ...
# rtctest.c:129:alarm_alm_set:skip alarms are not supported.
#            OK  rtc.alarm_alm_set
ok 4 rtc.alarm_alm_set
#  RUN           rtc.alarm_wkalm_set ...
# rtctest.c:185:alarm_wkalm_set:skip alarms are not supported.
#            OK  rtc.alarm_wkalm_set
ok 5 rtc.alarm_wkalm_set
#  RUN           rtc.alarm_alm_set_minute ...
# rtctest.c:231:alarm_alm_set_minute:skip alarms are not supported.
#            OK  rtc.alarm_alm_set_minute
ok 6 rtc.alarm_alm_set_minute
#  RUN           rtc.alarm_wkalm_set_minute ...
# rtctest.c:287:alarm_wkalm_set_minute:skip alarms are not supported.
#            OK  rtc.alarm_wkalm_set_minute
ok 7 rtc.alarm_wkalm_set_minute
# PASSED: 7 / 7 tests passed.
# Totals: pass:7 fail:0 xfail:0 xpass:0 skip:0 error:0
```

