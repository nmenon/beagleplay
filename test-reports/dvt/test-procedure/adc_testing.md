ADC Testing
===========

Test 0: Check if iio device is detected:

```
$ iio_info -S
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
Available contexts:
	0: (adc102s051 on Da AM625 Laughing Coyote Board) [local:]
```

Test 1: test low:

(connect a header as in following image)
![Test Low](img/adc_lo.jpg?raw=true "ADC Low")

```
debian@BeagleBone:~$ iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.140+ #1 SMP PREEMPT Tue Oct 11 20:00:02 CDT 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.140+
	uri: local:
IIO context has 1 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4080
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 0
				attr  1: scale value: 0.805664062
		No trigger on this device
```

Test 2: test High:

![Test High](img/adc_hi.jpg?raw=true "ADC High")

```
debian@BeagleBone:~$ iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.140+ #1 SMP PREEMPT Tue Oct 11 20:00:02 CDT 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.140+
	uri: local:
IIO context has 1 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 3804
				attr  1: scale value: 0.805664062
		No trigger on this device

```
