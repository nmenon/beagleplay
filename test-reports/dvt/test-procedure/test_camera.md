Testing Camera
==============

Prereqs
-------
* [Cable](https://smile.amazon.com/dp/B076Q595HJ/) [cable](https://www.amazon.com/Arducam-Raspberry-Camera-Ribbon-Extension/dp/B085RW9K13/) or [cable](https://www.amazon.com/Onyehn-Raspberry-Camera-Cable-Ribbon/dp/B07XZ5DX5H/) - Longer ones are known to create signal integrity issues - shorter the better.
* [Camera](https://smile.amazon.com/dp/B096XNS4C9/)

Sensor module:

> :warning: Notice the crystal - at times with other ov5640 modules, you'd have to dig up the datasheet and update the crystal frequency in the devicetree overlay for the sensor or worst case probe the crystal to find the crystal frequency

![camera module front](img/cam-module-front.jpg?raw=true "Camera module front")
![camera module back](img/cam-module-back.jpg?raw=true "Camera module Back")


![camera module setup](img/camera-setup.jpg?raw=true "Camera module setup")
![camera cable board side](img/cable-board-side.jpg?raw=true "Camera module Board Side")
![camera cable towards sensor](img/cable-sensor-side.jpg?raw=true "Camera module Sensor Side")

Enabling the camera:
--------------------
Edit the file /boot/firmware/extlinux/extlinux.conf
and add the following line just after fdtdir

```
 fdtoverlays /overlays/k3-am625-lc-csi2-ov5640.dtbo
```

Example for emmc boot:

```
label Linux eMMC
    kernel /Image
    append quiet quiet quiet console=ttyS2,115200n8 earlycon=ns16550a,mmio32,0x02800000 root=/dev/mmcblk0p2 ro rootfstype=ext4 rootwait net.ifnames=0 quiet
    fdtdir /
    fdtoverlays /overlays/k3-am625-lc-csi2-ov5640.dtbo
    initrd /initrd.img
```

Testing Camera
--------------

Open a terminal on the GUI

```
sudo apt-get install -y mplayer
mplayer tv:// -tv driver=v4l2:device=/dev/video0:width=640:height=480:fps=30:outfmt=yuy2
```

At a basic level, test can be performed to capture the YUV data itself for analysis later on for a frame by frame basis

```
mkdir camera_capture;cd camera_capture

yavta -c10 -Fframe-#.bin -f UYVY -s 640x480 /dev/video0
```

This generates frame-...bin files which can then be analysed with [7yuv](http://datahammer.de/) for frame by frame analysis.


Switching sensor module frequency
---------------------------------

Typically (subject to manufacturer), it's 12mhz for alinx and pcam, 24mhz for technexion

In case you do have a camera ov5640 module with a different crystal frequency (Say 24MHz), then, modify:

arch/arm64/boot/dts/ti/k3-am625-lc-csi2-ov5640.dts

```
diff --git a/arch/arm64/boot/dts/ti/k3-am625-lc-csi2-ov5640.dts b/arch/arm64/boot/dts/ti/k3-am625-lc-csi2-ov5640.dts
index 468986f4095e..f89c93963dd3 100644
--- a/arch/arm64/boot/dts/ti/k3-am625-lc-csi2-ov5640.dts
+++ b/arch/arm64/boot/dts/ti/k3-am625-lc-csi2-ov5640.dts
@@ -16,7 +16,7 @@ __overlay__ {
                        clk_ov5640_fixed: ov5640-xclk {
                                compatible = "fixed-clock";
                                #clock-cells = <0>;
-                               clock-frequency = <12000000>;
+                               clock-frequency = <24000000>;
                        };
                };
        };

```
